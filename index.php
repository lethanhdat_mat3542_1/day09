<!DOCTYPE html>
<html lang="en">
<?php include('./qs.php') ?>


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Programming Day02</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="./style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js">
    </script>
</head>

<body>
    <?php
    $page = "1";
    $temp = [];
    $ans2 = [];
    $string = file_get_contents('qs.json');
    $json_parse = json_decode($string, true);
    if (isset($_GET['page']) && !empty($_GET['page'])) {
        $page = $_GET['page'];
        if (isset($json_parse[$page])) {
            $json_parse = $json_parse[$page];
        } else {
            $json_parse = $json_parse[intval($page) - 1];
        }
        foreach ($json_parse as $key => $qs) {
            if (!empty($_GET['checkbox' . $key])) {
                array_push($temp, $_GET['checkbox' . $key]);
                array_push($ans2, $_GET['checkbox' . $key]);
            } else {
                array_push($temp, 'Rỗng');
                array_push($ans2, 'Rỗng');
            }
        }
        if ($page != "3") {
            setcookie('page' . strval(intval($page) - 1), json_encode($temp), time() + 3600);
        }
    } else {
        $json_parse = $json_parse[$page];
    }
    ?>
    <div class="wrapper">
        <?php
        if ($page != "3") {
        ?>
            <form method="GET" action="index.php">
                <div class="container">
                    <div class="row">
                        <?php
                        foreach ($json_parse as $key => $qs) {
                            qs($key, $qs);
                        }
                        ?>

                    </div>
                </div>
                <div class="button-box ml-3">
                    <?php
                    if ($page != "2") {
                        echo '<button type="submit" value=' . strval(intval($page) + 1) . ' name="page" class="button hover"> Next </button>';
                    } else {
                        echo '<button type="submit" value=' . strval(intval($page) + 1) . ' name="page" class="button hover"> Nộp bài </button>';
                    }
                    ?>
                </div>
            </form>
        <?php
        } else {
            $check = json_decode($string, true);
            $numRight = 0;
            for ($x = 1; $x < intval($page); $x++) {
                if (isset($_COOKIE['page' . $x])) {
                    $ans1 = json_decode($_COOKIE['page' . $x]);
                }
                $ans = [
                    "1" => $ans1,
                    "2" => $ans2
                ];
                foreach ($check[$x] as $key => $qs) {
                    if ($qs['solution'] == $ans[$x][$key]) {
                        $numRight++;
                    }
                }
            }
        ?>
            <div class="container">
                <div class="row">
                    <p>Số câu trả lời đúng của bạn là: <b> <?php echo $numRight . '<br/>'; ?> </b></p>
                </div>
                <div class="row">
                    <?php
                    if ($numRight < 4) {
                        echo " Bạn quá kém, cần ôn tập thêm ";
                    } else if ($numRight >= 4 && $numRight <= 7) {
                        echo " Cũng bình thường";
                    } else {
                        echo " Sắp sửa làm được trợ giảng lớp PHP  ";
                    }
                    ?>
                </div>
            </div>

        <?php
        }
        ?>
    </div>

</body>


</html>